---
layout: page
title: About
permalink: /about/
---

A curious engineer with over 16 years of experience in software development, where almost
6 years of his experience covers Android development, over 2 years covers teaching algorithms 
and programming in universities, and over 8 years covers academic and self-employed 
experiences while he was a student.

An Introvert person who has profound learning 
and contributing skills. A creative, value driven and passionate developer who enjoys 
doing math, solving problems and creating scalable, maintainable, testable and efficient 
apps to make the world a better place to live.

A person whose goal is to surround 
himself with happy, creative, motivative and clever persons, so his talent becomes 
flourished.

He's interested in Data Science, Machine Learning, Functional Programming, 
Differentiable Programming, Category Theory, Math, etc. so constantly looking forward to 
learning related technologies to use in Mobile apps."


