---
layout:     post
title:      Imagine World line — Big Picture
date:       2020-10-28
author:     Hadi Lashkari Ghouchani
summary:    This article wants to help you imagine the World line of particles. Generally, it’s a line from the past to the future of a specific...
categories: Future
thumbnail:  
tags:
- Physics
- Future
- Imagination
- Sketch-Theory
---

![Wallpaper](https://hadilq.gitlab.io/blog/assets/2020-10-28-imagine-world-line-big-picture-wallpaper.jpeg)

This article wants to help you imagine the [World line](https://en.wikipedia.org/wiki/World_line) of particles. Generally, it’s a line from the past to the future of a specific particle. Also I’m sick of watching movies that don’t make sense! They’re still following understanding of time from [Back to the Future](https://en.wikipedia.org/wiki/Back_to_the_Future) or [Terminator Genisys](https://en.wikipedia.org/wiki/Terminator_Genisys), etc! My problem with those movies is the way they deal with [The grandfather paradox](https://en.wikipedia.org/wiki/Grandfather_paradox).

> The grandfather paradox is a potential logical problem that would arise if a person were to travel to a past time. The name comes from the idea that if a person travels to a time before their grandfather had children, and kills him, it would make their own birth impossible. — Wikipedia

Those movies’ logic is simple, what happens in the past would rewrite the future, but what if something changes the cause that has been produced itself! Then like in the _Back to the Future_ they start to disappear! What?!

By the way, the picture that I want to draw is also simple and I hope writers read this article before they create another nonsense! However, if you are a writer and someone gives you the link to this article you may want to just jump to _For Writers_ section, because until then I would draw a sketch of a serious scientific paper! Also be ready to rethink what you already know without thinking that an amateur Physicist solved this puzzle, as here we will not talk about totally new concepts. Just old stuff with new arrangements to make a consistent picture. So here we are!

# Assumptions

## Second law of thermodynamics
Let [Second law of thermodynamics](https://en.wikipedia.org/wiki/Second_law_of_thermodynamics) be correct.

> The second law of thermodynamics states that the total [entropy](https://en.wikipedia.org/wiki/Entropy) of an [isolated system](https://en.wikipedia.org/wiki/Isolated_system) can never decrease over time, and is constant if and only if all processes are reversible. — Wikipedia

Everyone knows that it’s correct. Just break an egg and try to bring it back like before breaking.

Here we think of thermodynamics entropy as [Shannon entropy](https://en.wikipedia.org/wiki/Entropy_\(information_theory\)), because they have the same formula to calculate. As Shannon entropy is a measure of information in the system, this also means the total information of an isolated system can never decrease over time. I know people would argue that thermodynamics entropy is not information, but I would say deal with the fact that it’s information. Please! Also I can continue like this. As you can see in the above definition, being constant for total entropy of an isolated system means the process is _reversible_. On the other hand, in classical theories we have an official argument. It says conservation of information for instance in [Newtonian Mechanics](https://en.wikipedia.org/wiki/Classical_mechanics) means being _reversible_ in time. Where they actually talk about the Second law of Newtonian Mechanics and change of `t` to `-t` to see the equation is not changing. Or in Quantum mechanics take [Schrödinger equation](https://en.wikipedia.org/wiki/Schr%C3%B6dinger_equation) and do the same with t, to see the equation is not changing. They call it [conservation of information](https://en.wikipedia.org/wiki/Black_hole_information_paradox), but also they will argue this information is not that information! mmm! Not acceptable!

Anyway, they behave the same, so in this article we accept that we have just one definition of information, so _Shannon entropy_, thermodynamics entropy, and information are all the same.

In reality, time is not reversible because generation of information is the clock ticks.

## Many-worlds
Let the [Many-worlds interpretation](https://en.wikipedia.org/wiki/Many-worlds_interpretation) of quantum mechanics be the correct interpretation.

> The many-worlds interpretation (MWI) is an [interpretation of quantum mechanics](https://en.wikipedia.org/wiki/Interpretations_of_quantum_mechanics) that asserts that the [universal wavefunction](https://en.wikipedia.org/wiki/Universal_wavefunction) is objectively real, and that there is no [wavefunction collapse](https://en.wikipedia.org/wiki/Wavefunction_collapse). This implies that all [possible outcomes](https://en.wikipedia.org/wiki/Possible_world) of quantum measurements are physically realized in some "world" or universe. — Wikipedia

As far as I know, this is the only consistent interpretation of quantum mechanics as it accepts the fact that subatomic particles would entangle with the environment around them, so it’s unavoidable to think the entire universe is out of the scope of entanglement. If you ignore this fact, you would encounter [paradoxes](https://www.space.com/quantum-paradox-throws-doubt-on-observed-reality.html). If you accept it, you’ll end up with MWI.

# Pattern

Here we need to describe what the world line looks like. To do so, we need a model to describe it, so if we define a model, we could prove it’s behaving like what our assumptions imply.

## Lightning Pattern

In thunderstorms, there is ordinary lightning like the image below.

![Borrowed from https://www.treehugger.com/types-of-lightning-4864273](https://hadilq.gitlab.io/blog/assets/2020-10-28-imagine-world-line-big-picture-lightning.jpeg)

Here we want to mathematically describe it. First you can notice it’s a graph. So you may think the tree graph is the structure of this lightning. But it’s a little bit more complicated. The lightning in the picture above, is a discharge of negative charges from clouds to the ground. So basically negative charges, aka electrons, move toward the electric field, to find the ground and hit it. In their _search_ for the ground, these electrons hit the air molecules and change their directions. Also the electric field is not a constant vector there, so electrons cannot find the straight direction to the ground. The result of this [Trial and error](https://en.wikipedia.org/wiki/Trial_and_error) process of _searching_ is that some branches would fail to hit the ground. But finally one branch will find the ground and all electric charges will discharge over that branch, where you can find that branch more bright in the above picture. Lets name them. Call the failed branches _dead-end_ branches, and the successfully found the ground branch, the _final_ branch. We should mention there is no guarantee to stop those electrons to _join_ each other again in their _search_ path. Which mathematically avoids us to call that structure a tree graph. Because in a tree, branches cannot _join_.

Let’s define the _lightning graph_. It's a [Directed graph](https://en.wikipedia.org/wiki/Directed_graph), where globally is a tree graph, but locally can have loops. Also the direction in this graph is from root to leaves.

# Quantum Mechanics

In quantum mechanics, we describe a system by quantum states. For instance, an electron beam has a state, where it shows the direction of propagation, momentum, and how it’s a wavefunction. So after the electrons in the beam pass the [Double slit](https://en.wikipedia.org/wiki/Double-slit_experiment) they produce a wave pattern, the same as light waves would produce it. But in case of quantum states, when they hit the screen they have to follow the eigenstates of the screen system, so based on the _MWI_ they would fall into different worlds depending on different eigenstates of the screen. Where in each world an electron would create a dot, respect to the eigenstates that it falls to, on the screen.

Did you notice we didn’t have to talk about observer or measurement, or the [Wave–particle duality](https://en.wikipedia.org/wiki/Wave%E2%80%93particle_duality) in above description. Just the eigenstates of the screen are different from the eigenstates of the empty space, so electrons have to project their state in the [Hilbert space](https://en.wikipedia.org/wiki/Hilbert_space), where those states lives, to the new system(screen system) and create worlds(branches). Also only wave behavior is needed in this description. No particles. Just dots, which are the eigenstates of the screen system. Also notice in lack of particles, the world lines would be tangent to the path of information movement.

Let’s talk more about Quantum observers. In all the official interpretations of Quantum Mechanics, measurement forces the existence of observers that are different from simple particles, which is contradicted with [Special Relativity](https://en.wikipedia.org/wiki/Principle_of_relativity) Principles

> If a system of coordinates `K` is chosen so that, in relation to it, physical laws hold good in their simplest form, the same laws hold good in relation to any other system of coordinates `K’` moving in uniform translation relatively to `K`. — Albert Einstein

Read coordinates `K` and coordinates `K’` as observation of particle `A` and observation of particle `B`. Also read a particle as a dot on a world line. Before, we already mentioned that world lines are tangent to the path of information movement. This implies every particle are observers. Which in fact is consistent with the above description of _Double slit experiment_. You may call it a new interpretation, but in my opinion, it’s just _Special Relativity Principle_. So now that we satisfied the compatibility of _MWI_ with classical mechanics, which means world lines are a thing in Quantum mechanics, we can come back to Double slit experience again.

What would happen to all those worlds that have been created by hitting the screen by electrons? Their outcomes are different and they make dots in different positions of the screen, so all those worlds are created, but the environment doesn’t care about the position of a dot of an electron on the screen. It cares more about the pattern a bunch of electrons draw on the screen. By environment, we mean the rest of the world. Nothing would change if the dots’ position were different, because the pattern on screen is still the same, so the created worlds would _join_ together afterwards.

In other words, the electron world line would create sibling branches, when it hits the screen, and would merge afterwards with sibling branches to become one branch.

What if we split the screen in two sides and say if the electron hit the right side we will kill Hitler(Assume we’re in 1920!) and if it hit the left we will do nothing. In that case the electron’s world line who hit the right side would merge together. Also the same would happen to the electron’s world line of the left side. So in the end, we have two distinct world lines from initially one world line of an electron. Then two distinct branches would be created. Does it remind you of [Schrödinger’s cat](https://en.wikipedia.org/wiki/Schr%C3%B6dinger%27s_cat)? me too! But imagine someone did this with Alexander the Great, so we have enough time for branches to completely separate! Do you think for an observer on the Andromeda Galaxy world lines of left and right sides of the screen would be different? In its perspective the both branches have been merged. So when you watch a system globally enough you would see a lot of the world lines are merged together. On the other hand, if you observe the screen locally enough, all the world lines of different dots would be separated.

World lines are totally directed lines as they start from the past and end to the future. Also as we argued above they would merge together if you observe them globally enough, which means there are local joins.

# Evolution

[Evolution](https://en.wikipedia.org/wiki/Evolution) is described like this

> This is followed by three observable [facts](https://en.wikipedia.org/wiki/Fact) about living organisms: (1) traits vary among individuals with respect to their morphology, [physiology](https://en.wikipedia.org/wiki/Physiology) and behaviour ([phenotypic variation](https://en.wikipedia.org/wiki/Phenotype#Phenotypic_variation)), (2) different traits confer different rates of survival and [reproduction](https://en.wikipedia.org/wiki/Reproduction) (differential [fitness](https://en.wikipedia.org/wiki/Fitness_(biology))) and (3) traits can be passed from generation to generation ([heritability](https://en.wikipedia.org/wiki/Heritability) of fitness). — Wikipedia

In my understanding of these facts, they just want to say _Evolution_ is a _search_ for better fitness to the environment. Hah! Is fitness related to increasing information? To answer let’s checkout Shannon entropy, which is a measure for information. It can be easier to understand it if you think of it as _uncertainty_. For instance, assume we produce two pens which are exactly the same. We name the pens one and two, then randomly give those pens to Alice and Bob. How _certainly_ Alice can answer that her pen is the one? How much she is _uncertain_ about the answer, we’ve created more information. Notice the creation of information happens when we produce two identical pens, but the uncertainty happens after we distribute the pens. But they are different ways to think about the same thing.

After all, factories, given their products are identical, are actually information generators (Based on the second law of thermodynamics, that’s why it looks like time ticking in favor of Capitalism! haha! Sorry! We have to be serious in this part!). Species are factories for their kind, so they are information generators, but there are limits pushed by the environment to restrict their production, such as lack of resources, other species, etc. On the other hand, time wants to tick, so species change themselves to fit more to the environment to produce more and more to make the time tick. Notice that when we say to make the time ticks, it actually means time ticks in the direction to produce more.

This clarifies what’s happening in the dead-end branches. Those branches are the ones where production is stopped so time is not ticking there, because in their search they reach the state of not being productive. Which means they don’t increase the information so time would not go forward. So these conditions imply that world lines are matches for lightning pattern as defined in the previous section.

You can think that maybe in some universes/worlds dinosaurs are still alive, if they would still be productive. In the end we cannot conclude that. Maybe our universe is the _dead-end_ one!

The takeaway here is that the _Evolution_ is the process of Trial and error, just like lightning. They both are the same algorithm for searching.

# Big Picture

So the question is where this lightning pattern lives? As we’re in the realm of Quantum mechanics and every state in Quantum mechanics lives in [Hilbert space](https://en.wikipedia.org/wiki/Hilbert_space) so the [Hilbert manifold](https://en.wikipedia.org/wiki/Hilbert_manifold) is where this beast lives. This picture is looking clear when you replace electric fields as a guideline for electrons in lightning with increasing entropy direction in the _Hilbert manifold_.

Be aware that world lines originally defined to be in 3+1 dimensional space-time, but here we talk about world lines in _Hilbert manifold_. In both definitions a world line comes from the past and goes to the future.

However the question that you may think of is if for each branch the whole world gets copied, then what would happen to the conservation of energy-momentum? The answer is the energy-momentum lives in the space-time manifold and those branches live in _Hilbert manifold_. Those manifolds are perpendicular to each other and branching in one would not affect the other.

There is still another question. Does the branching of a whole world happen instantly? This is a tough one. We don’t know, but instantly doesn’t mean the whole world gets copied for the new branch. It can be a lazy copy, where for the observers in those worlds it looks like the branching is instant but actually whenever a different measurement happens in the new branch the information gets copied there.

Here we want to ask an important question, which makes all these sense. How can we measure these worlds to make this scientifically acceptable.

As far as the author can think of, the only way to measure them is to make a time machine. A time machine would move a world line to intersect with itself in the past. Quantum mechanically, where that intersection happens, the system in the intersection point would change in the way to have new eigenstates in the _Hilbert space_, which means new branches in the lightning pattern would be created. As you can see in this model the time traveler would never reach the _The grandfather paradox_, because the time traveler would create brand new branches to live there. So no erase, no rewrite, no paradox.

# Time machines

Let’s start from [Complexity classes](https://en.wikipedia.org/wiki/Complexity_class).

> In [computational complexity theory](https://en.wikipedia.org/wiki/Computational_complexity_theory), a complexity class is a [set](https://en.wikipedia.org/wiki/Set_(mathematics)) of [computational problems](https://en.wikipedia.org/wiki/Computational_problem) of related resource-based [complexity](https://en.wikipedia.org/wiki/Computational_complexity). The two most commonly analyzed resources are [time](https://en.wikipedia.org/wiki/Time_complexity) and [memory](https://en.wikipedia.org/wiki/Space_complexity). — Wikipedia

In these classes there is a [P](https://en.wikipedia.org/wiki/P_versus_NP_problem) class that classical computers can solve. Classical computers are working imperatively and sequentially. We can think of steps of solving a problem in those machines like dots on a single line(world line). Also there is a complexity class named [BQP](https://en.wikipedia.org/wiki/BQP).

> In [computational complexity theory](https://en.wikipedia.org/wiki/Computational_complexity_theory), bounded-error quantum polynomial time (BQP) is the class of [decision problems](https://en.wikipedia.org/wiki/Decision_problems) solvable by a [quantum computer](https://en.wikipedia.org/wiki/Quantum_computer) in [polynomial time](https://en.wikipedia.org/wiki/Polynomial_time), with an error probability of at most 1/3 for all instances.

Where mathematicians [prove](https://www.quantamagazine.org/finally-a-problem-that-only-quantum-computers-will-ever-be-able-to-solve-20180621/) that all P problems are inside BQP, but there are problems in BQP that are not in P. If a classical computer tries to solve those problems, it would take infinite time to solve it, where a Quantum computer can solve them in finite time. This means Quantum computers are not solving it like the dots on a single line. A Quantum computer can have a more complex pattern of dots on multiple lines, which means they can loop back in time. Quantum computers are initial versions of time machines. However, this is just one way of interpreting the power of Quantum computers, but there are other ways to think about it without time loops. I just emphasize here that with a great chance time travel is possible and we will experimentally prove it.

# Anthropic principle

> The anthropic principle is a group of principles attempting to determine how statistically probable our observations of the universe are, given that we could only exist in a particular type of universe to start with — Wikipedia

The picture that we come up with until here can explain why the world is really [fine-tuned](https://en.wikipedia.org/wiki/Fine-tuned_universe). This makes our existence more probable by merging worlds and makes a lot of branches _dead-end_ as described in previous sections. After all, it’s a search algorithm so it can tune some constants and events. Also it let us go further. This picture is not even making our life more probable with respect to fundamental physical constants, it also answers why there was no other massive comet to hit the Earth after [Cretaceous–Paleogene extinction](https://en.wikipedia.org/wiki/Cretaceous%E2%80%93Paleogene_extinction_event) event or why we’re in a branch that world war II ended soon enough so we’re still alive! All because our world is still productive.

# For Writers

This is the section that I let my imagination fly. To honored writers who are looking for consistency, the [Edge of tomorrow](https://en.wikipedia.org/wiki/Edge_of_Tomorrow) is the correct pattern to follow. The whole war scene is basically a Quantum computer to calculate how _Omega_ can win the war. It’s exactly a _BQP_ decision problem. However time travel of information inside the head of _Tom Cruise_ without any equipment looks a little weird! ;)

For those who grasp the concept I have a new nonsense theory, which is good for sci-fi movies. If time travel is possible and the traveler would create a new branch then there is a possibility that we are already inside a man made branch. Then can you think of any scenario? I can! The [Egyptian pyramid construction techniques](https://en.wikipedia.org/wiki/Egyptian_pyramid_construction_techniques) are a mystery as there is no way human technology of that age could have made them! So, people from the future built them for a reason that can have different scenarios and I don’t care here. But time travel can fix the problem of their construction techniques.

I have another perfect scenario! Imagine in the future some elite society who has time machines and ability to keep people alive as much as they want or the rules allowed. They want to colonize the Galaxies but they need good seeds to spread. By good seeds, I mean good information. Part of the information comes from science, technology, society, etc. that that society already has and can spread them. The other information will be genetic information that deserves to be spread everywhere. However, given that they have time machines, to have access to their best genome information, they can look over all times and choose good genomes. But what are good genomes? For sure they will look for the genomes who actually build the ground for their science and technology, so they can do the same all over the galaxies, in case of new problems. For instance, Newton had no child, but he deserved to be colonized everywhere. Even now, if every company that uses his theories gives him a small percent of their revenue for every time they use them, he would be the most rich person on the planet. Not that he just deserved to be spread, the elite society needs him to do so. Therefore, the plan will be to replace a fake corpse with the real science and technology legends of the history in their final moment of life, to keep them alive and avoid new branches in that moment. If I was a religious guy I would say that it’s heaven for those legends! haha! Although it doesn’t support justice as the religions have been promised! haha! It’s based on economy and information resources.

It was the final joke that I had. haha! What other scenario can you build with this?

# References

- [World line](https://en.wikipedia.org/wiki/World_line)
- [Terminator Genisys](https://en.wikipedia.org/wiki/Terminator_Genisys)
- [What Is the Grandfather Paradox?](https://www.space.com/grandfather-paradox.html)
- [Grandfather paradox](https://en.wikipedia.org/wiki/Grandfather_paradox)
- [Back to the Future](https://en.wikipedia.org/wiki/Back_to_the_Future)
- [Second law of thermodynamics](https://en.wikipedia.org/wiki/Second_law_of_thermodynamics)
- [Shannon entropy](https://en.wikipedia.org/wiki/Entropy_\(information_theory\))
- [Newtonian Mechanics](https://en.wikipedia.org/wiki/Classical_mechanics)
- [Schrödinger equation](https://en.wikipedia.org/wiki/Schr%C3%B6dinger_equation)
- [Black hole information paradox](https://en.wikipedia.org/wiki/Black_hole_information_paradox)
- [Many-worlds interpretation](https://en.wikipedia.org/wiki/Many-worlds_interpretation)
- [Copenhagen Interpretation](https://en.wikipedia.org/wiki/Copenhagen_interpretation)
- [New quantum paradox throws the foundations of observed reality into question](https://www.space.com/quantum-paradox-throws-doubt-on-observed-reality.html)
- [Trial and error](https://en.wikipedia.org/wiki/Trial_and_error)
- [Directed graph](https://en.wikipedia.org/wiki/Directed_graph)
- [Double slit experiment](https://en.wikipedia.org/wiki/Double-slit_experiment)
- [Wave–particle duality](https://en.wikipedia.org/wiki/Wave%E2%80%93particle_duality)
- [Hilbert space](https://en.wikipedia.org/wiki/Hilbert_space)
- [Special Relativity Principles](https://en.wikipedia.org/wiki/Principle_of_relativity)
- [Schrödinger’s cat](https://en.wikipedia.org/wiki/Schr%C3%B6dinger%27s_cat)
- [Evolution](https://en.wikipedia.org/wiki/Evolution)
- [Hilbert space](https://en.wikipedia.org/wiki/Hilbert_space)
- [Hilbert manifold](https://en.wikipedia.org/wiki/Hilbert_manifold)
- [Complexity classes](https://en.wikipedia.org/wiki/Complexity_class)
- [P versus NP problem](https://en.wikipedia.org/wiki/P_versus_NP_problem)
- [BQP](https://en.wikipedia.org/wiki/BQP)
- [Finally, a Problem That Only Quantum Computers Will Ever Be Able to Solve](https://www.quantamagazine.org/finally-a-problem-that-only-quantum-computers-will-ever-be-able-to-solve-20180621/)
- [Fine-tuned universe](https://en.wikipedia.org/wiki/Fine-tuned_universe)
- [Cretaceous–Paleogene extinction event](https://en.wikipedia.org/wiki/Cretaceous%E2%80%93Paleogene_extinction_event)
- [Edge of tomorrow](https://en.wikipedia.org/wiki/Edge_of_Tomorrow)
- [The Egyptian pyramid construction techniques](https://en.wikipedia.org/wiki/Egyptian_pyramid_construction_techniques)

# Original Post
This article originally posted on [Medium](https://medium.com/predict/imagine-world-line-big-picture-9ba946b261f7).

