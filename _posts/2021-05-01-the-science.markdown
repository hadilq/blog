---
layout:     post
title:      The Science
date:       2021-05-01
author:     Hadi Lashkari Ghouchani
summary:    What is the Science? Beside very clever minds defined Science before, but it's a scientific way to be skeptic about them...
categories: Science
thumbnail:
tags:
- Science
- Bio
---

![https://en.wikipedia.org/wiki/File:Fractal_dragon_curve.jpg](https://hadilq.gitlab.io/blog/assets/2021-05-01-the-science-wallpaper.jpeg)


What is the _Science_? I feel the definition of _Science_ is too complicated that can cause a lot of problems. In current era, it looks like a new kind of religion to some people. At least, folks compare it with religions without noticing they are actually comparing them! They change their everyday life style to what _Science_ is discovering. Shouldn't they just update it gradually and celebrate their freedom? Politicians try to control it in their safe zone like other religions! Do I need to mention the flow of money to universities to prove my point? Any way, why is the _Science_ important? Who are the _Scientists_?

Beside very clever minds defined _Science_ before, but it's a scientific way to be skeptic about them and try to create your own version. Hopefully it will look more consistent. So let me define it again!

As a side note, if you see ANY INCONSISTENCY please let me know, because it's probably the way that I explained it here, not the idea itself, because this idea passed a lot of tests in the last ten years of my life, but in the end, I am just one person.

There is a chance that people already did all this definitions and conclusions before and I just didn't have a chance to read about them, but it will not bother me as I have a lot of similar experiences before. For instance, in the end of high school I have found a very beautiful set of relations in geometry, that later in my bachelor degree I find out people actually call them [Christoffel symbols](https://en.wikipedia.org/wiki/Christoffel_symbols). So I use to these kind of experiences, but please guide me if you think you have heard of these ideas any place else.

# What is Science?

Based on Wikipedia [Science](https://en.wikipedia.org/wiki/Science) is

> Science (from the Latin word scientia, meaning "knowledge")[1] is a systematic enterprise that builds and organizes knowledge in the form of [testable](https://en.wikipedia.org/wiki/Testability) [explanations](https://en.wikipedia.org/wiki/Explanation) and [predictions](https://en.wikipedia.org/wiki/Prediction) about the [universe](https://en.wikipedia.org/wiki/Universe). - Wikipedia

Also sometimes Science defines with the [Scientific Method](https://en.wikipedia.org/wiki/Scientific_method) algorithm. It goes like this

> The scientific method is an [empirical](https://en.wikipedia.org/wiki/Empirical_evidence) method of acquiring [knowledge](https://en.wikipedia.org/wiki/Knowledge) that has characterized the development of [science](https://en.wikipedia.org/wiki/Science) since at least the 17th century. It involves careful [observation](https://en.wikipedia.org/wiki/Observation), applying rigorous [skepticism](https://en.wikipedia.org/wiki/Skepticism) about what is observed, given that [cognitive assumptions](https://en.wikipedia.org/wiki/Philosophy_of_science#Observation_inseparable_from_theory) can distort how one interprets the [observation](https://en.wikipedia.org/wiki/Perception#Process_and_terminology). - Wikipedia

In my mind it's so hard to understand above statements because after reading them I have to go searching for testability, explanations, predictions, universe, etc. It's hard! Complicated! Easy to make fallacies. What is a fallacy by the way?! Let's search! 

Another big reason that it's not working as expected is the [Demarcation Problem](https://en.wikipedia.org/wiki/Demarcation_problem), where it's impossible to define a good line to separate what is scientific and what is non-scientific. We try to make it clear here.

We're looking for an easy sentence to define it. We don't want to assume anything. We want to start building science and math from ground up. So let's start assumption-less. Also we expect that it must be agreeable by every honest person. Here is my effort.

> Science is the repeatable observed data from our surrounding.

It's simple after all. A toddler can see his/her surrounding so it's assumption-less. A kid knows what is observation and repeatable, also a teen knows what is data. Someone would say there is _Knowledge_ hidden in the _Science_ term itself, so where is that in your definition? Well! Here we say, we ignored that because that will make _Science_ inconsistent, incomplete, paradoxical, etc. So basically here _Science_ is separated form human being's _Knowledge_. On the other hand, we expect _Science_ be our common language for truth, where we know it's correct and we can agree on that. We cannot be agreed on our _Knowledge_. So by this new definition we can all be agree on _Science_ as we expected from this term. That's why we didn't use another term for this statement, instead we used _Science_. It satisfies what we are looking for. After all our Knowledge exist and here we will call it _Knowledge_, not _Science_.

# Construction of Knowledge

After defining the _Science_, we need to rediscover its relationship with our _Knowledge_. To do so, here we will add assumptions one by one to build it up. The following is a summary of what we want to do and we will come back to it in details later.

This story starts from the equipment that we observe our surrounding by them. We need basic concepts of logic to record our observed data. After that we use transformations and regressions to create models, theories and abstractions for the data that we have recorded before. Then, we can name our data and their abstractions to create languages. After that, we will go to more and more abstraction and each layer of abstraction would push us more and more apart from the _Science_, therefore, the disagreements start to grow and grow more. Then we can move  more in the abstraction direction. There are Philosophy, Astrology, religions, then conspiracy theories, etc. when you move toward on abstraction direction, where all of us are disagree in some details about these abstractions even with what is in the brain of our closest persons in our life! I suppose that would cover all of human's _Knowledge_.

That's the whole picture of our _Knowledge_ that we extracted from _Science_. Be aware that we couldn't possibly gain this _Knowledge_ from any other source. Since we were a kid, we gathered data from our surrounding and created abstractions upon them. Generation after generation of doing that created all of human's _Knowledge_. Then there is no other source to start with.

Let's talk more about this single source of truth. The big question is why we're all agree about the raw observed data? I'm trying to answer that here. Because of one reason, we don't have another choice. All we knew, know and will ever know are based on our observations. In the end, the single source of truth is the _Science_ and every other things in the _Knowledge_ are abstractions from it, so I challenge you to name one thing(thought, imagination, creation, dream, gods, etc.) that is not some kind of abstraction based on some observations. After you named it, I will separate it into details of abstractions and show you there is actually some observations related to those abstract parts. For instance, unicorn is based on abstractions of pony and goat, which both are observables! What about nothing? Nothing is based on abstractions of empty set, where is also abstracted your empty shopping cart, etc., and the fact that zero is before one on the `x` axis. What about something that doesn't exist? Something that doesn't exist is based on abstraction of nothing and the fact that minus one is before zero on the `x` axis. As you see, the examples are not observable, but can be analyzed to their abstract parts, which are totally related to some observations. Hope the point is clear by now.

Let's explain that personal _Knowledge_ is something a little bit more than the human _Knowledge_, where is not forced to be agreeable. So when we say _Science_ is the single source of truth for human _Knowledge_, don't try to find a piece of your personal _Knowledge_ that you cannot explain to anyone else as an example that contradicts to it! As soon as you start to explain it or define it, your listener can apart those abstractions to find the original observations.

So basically we are forced to accept that one choice. Therefore, better be honest with ourselves and others to accept our agreement on _Science_. By the way, I would define honesty itself by observed data, aka _Science_, but you would complain that I am changing all the definitions at once!

So far we have a rock surface of agreeable data, where we call them _Science_, then there is a direction perpendicular to that surface where if we move along that the disagreement grows and all of human _Knowledge_ is there. Nothing is outside of this picture!

I just want to clarify [Demarcation Problem](https://en.wikipedia.org/wiki/Demarcation_problem) here, that there is not a clear boundary in the abstraction direction to separate the _Knowledge_ that we can include it in _Science_ and the _Knowledge_ which is outside of _Science_. This boundary assumed to exist in the previous definitions of _Science_ like the definition of being [Falsifiability](https://en.wikipedia.org/wiki/Falsifiability), but it's not as agreeable as it looks like. There are always some corner cases that cannot be fit into the boundary definition. That's why here we stick to the raw observed data as the _Science_, to have a crystal clear separation of what is _Science_, but as a side effect, there's no definition of a Scientific Theory here. Although, we know moving along the abstraction direction will make theories more non-scientific. Therefore, here we replaced previously assumed boundary with some numbers on abstraction axis to show how much we cannot rely on a theory.

It worth mentioning that we will use assumptions and abstractions interchangeably in this story, because to add an assumption to our mathematical system, it needs to be consistent with what we observed. So in each assumption there are some hidden relations with the observed data where we cannot directly extract them from the data, which forced us to guess them, and put them in our system directly and call them assumptions. Those relations are linking the assumptions with the observed data in a way that we can call them abstraction of some observed data. Therefore, each assumption is an abstraction and obviously each abstraction is an assumption.

Further in this story you will notice theories like [Newton's laws of motion](https://en.wikipedia.org/wiki/Newton%27s_laws_of_motion) that are considered before as Scientific theories actually holding a lot of assumptions/abstractions which makes them disagreeable! Notice that one or more of Newton's assumptions make Newton's theory more wrong respect to [General relativity](https://en.wikipedia.org/wiki/General_relativity). Can you name those assumptions? Also [General relativity](https://en.wikipedia.org/wiki/General_relativity) tolerates some assumptions/abstractions that can eventually be wrong. We don't want that when we want to talk about the rock surface named _Science_. We don't want to be wrong ever!

Here, the author wants to share with you the greatest good of this definition. You definitely heard that "_Science_ is always changing". Here, it's not! It's our agreement from past to the future. Whenever you think it's changed just setup the experiment with the same instruments and environment, which means their accuracy included in the experiment, then you will get the same results. The same results [Galileo](https://en.wikipedia.org/wiki/Galileo_Galilei) achieved if you exactly setup his experiments. All the times. From past to the future. Yes! The number of observation will grow and the accuracy of experiments will increase, but what we already know from _Science_ will not change ever.

By the way, there are also testability, prediction, etc. that we didn't address in our definition. That's because there are some assumptions before we could talk about them.

# Observation

Before talking about testability, let's put all of our observed data in a table. Yes a table! You will smell a Data Scientist around here, but let's ignore him! Now we need a table, but a table is an abstract concept. So even from the start of recording our observed data we need some kind of abstractions/assumptions, but we restrict ourselves to these few assumptions to have a clear boundary between what is in this table and what is not. Let's list the first set of abstractions we need to record our data. First of all, we need to have [Sum Types](https://en.wikipedia.org/wiki/Tagged_union) because we can abstract every measurement on a gauge, ruler or any other measurement instrument as a _Sum Type_. A _Sum Type_ is what you can deduce the _or_ operator from it, but it's much stronger, where you can find it in the [Type Theory](https://en.wikipedia.org/wiki/Type_theory). Then we need [Product Types](https://en.wikipedia.org/wiki/Product_type) to create rows and columns of the table. A _Product Type_ also can be used to deduce the _and_ operator, but again, it's much stronger. You may noticed we just included the basics of logic in our assumption list.

So for each observation we have a row and for each gauge, ruler or instrument in that observation we have a column. Therefore some observed data have common columns and some have completely separated columns, but all we ever recorded of repeatable observations are in that table, so it's a giant table, but a finite one. Be careful that each row can have a lot of empty elements, because not for every observation we used all of our instruments to measure everything. The final feature of this table is a column for the type of setup of instruments, which means every experiment has one specific type for the arrangement of observers, where we can abstract it by a _Sum Type_. We'll call this column the experiment type.

We call this table _The Observation Table_ or _TOT_ and every honest person will be agree on it. So, as a reminder, it's the _Science_ itself.

However, the common columns are known to us because we used the same instruments in different observations to observe with them, therefore we are able to identify common columns. For instance, we use *a* clock to observe time, so by that we know all times that observed by that specific clock are belonging to *a* column. Also be careful that every clock we ever experiment with has their own unique column in _TOT_.

# Testability

Now let's assume our surrounding, including our body and brain, have a structure that repeats some patterns in them. By pattern we mean some columns for a specific type of experiment in _TOT_ are exactly the same where we ignored the rest of columns. What about errors? By exactly, we assumed there is a part of universe that is linear so errors will add up to real values, which implies by losing accuracy the values will be exact. But, we can add this assumption latter in the abstract direction, and for now just stick to less accurate instruments to avoid most of noises. Just like what our ancestors did. After all, not having access to the most accurate instruments in early stages was an advantage. Also notice, those patterns repeated almost on all rows of that specific experiment type. For instance, let's assume we can observe two boolean parameters in two different times. We ignore the time column and the same boolean parameters with the same order showed up in the two rows of different times. By Including the experiment type, that's a pattern if they repeated on all over _TOT_.

By this assumption we accepted that we live in a universe where patterns repeated a lot. In mathematics we call such structure a [Fractal](https://en.wikipedia.org/wiki/Fractal#), so I call this assumption, _Fractal Hypothesis_. Notice for building any fractal we need [Recursion](https://en.wikipedia.org/wiki/Recursion) so it's a part of this assumption and we will refer to it later. You'll see how important is this assumption, where we found it shining by defining Science and constructing _Knowledge_ from scratch.

We started assumption-less, then so far we assumed _[Sum Types](https://en.wikipedia.org/wiki/Tagged_union)_, _[Product Types](https://en.wikipedia.org/wiki/Product\_type)_ and _Fractal Hypothesis_. *These basic concepts of logic and _Fractal Hypothesis_ are enough assumptions to record _TOT_ and create _Science_.* Notice, to recording these observations we don't need to assume point and line, etc. 

Reader must be aware that the author is trying to explain the process with details, but he's sure there are more details in it. By the way, eventually there are practical ways to do it in the outside world which means it's a consistent process. For instance, dealing with errors is a bit more complicated than what explained above, but important point here is that there is a way to move from _TOT_ on the direction of abstraction. Also as a side note, don't forget that errors are a part of _TOT_, so they are as important and useful as the rest of _TOT_.

The other example of details here is how human observers, who recorded  _TOT_, are trustworthy, so can we trust _TOT_? It supposes to be the most agreeable thing we have. The possible answer can be as follows. By a trust network we already achieved the most agreeable thing in our _Knowledge_ today, which is the _TOT_, it's just not in one place, clean, with the notion of a table. Having in mind that _Fractal Hypothesis_ allows us to repair experiments of _TOT_ whenever we cannot find a way in our trust network to people who did the experiments. Again, there are practical ways to do it in outside world which means it's a consistent process.

We assumed _Fractal Hypothesis_ to build up _TOT_ but we ignored why. Here we want to explore more about that why. There are specific types of experiment that happened once in history. For instance, miracles of prophets are parts of history. If your mindset is similar to the author then history is not trustworthy, so those kind of observations are not included in _TOT_, no inconsistency, so let's move on. However, it worth mentioning that this kind of experiment shows even prophets used observation's agreeability to build their theories! By the way, even if you really trust those parts of history, then those single happened observations cannot be showed up in _TOT_, because they are not following _Fractal Hypothesis_ as they are not repeatable. This is an important roll that _Fractal Hypothesis_ plays to build up the _TOT_, the most agreeable thing we can talk about.

# Compressing

The _TOT_ table is a giant table and we cannot work with it. That's why we need abstractions. Because when we compress that table enough to fit inside our relatively tiny brain then we can say, aha, we understand the universe! Let's compress it to be more comprehensible. To do so, we need a huge number of abstractions upon abstractions to simplify part of that table to some concepts or formulas. Let's do it.

First we need to add more abstractions to our collection of abstractions/assumptions. Here we need functions. They can map _Sum Types_ to other _Sum Types_. Then we find out a lot of patterns in our observations have extra columns where a simple one to one relation can map them together. For instance, we have two different clocks in two different set of experiments. We observe that except the clock column the rest of columns are the same which means there is a pattern there. Also we see in one type of experiment, for each state in the _Sum Type_ of one clock's column there is exactly one state in the _Sum Type_ of the other clock's column and it's repeated on all rows of the same experiment type. Here we can define a standard unit for clocks, then map all the others to _Sum Type_ of one standard clock. Later in deeper abstractions we'll call this standard unit of time.

Now, we can do the same with our rulers and gauges to have standard units. In this step we can merge a lot of clocks' columns to each other. So compressing started with shrinking a lot of rows to other ones, because after merging columns a lot of rows become completely the same, where we can safely remove repeated ones. Let's name the new table _T<sub>1</sub>_. The `1` in this naming could be any positive real number, but let's stick to the simple one. Also we can name _TOT_ with the new notation, _T<sub>0</sub>_, to avoid future confusions. Therefore _T<sub>0</sub>_ is the new representation of _Science_ and it's only a part of classical definitions, because the classical definitions included both observations and some parts of human's _Knowledge_. 

The observation part is the _T<sub>0</sub>_ and the other parts start with _T<sub>1</sub>_, _T<sub>2</sub>_, etc. where shows the direction of abstraction. Of course to name them we need to wait to fall deeper in abstractions to invent the languages in _T<sub>L</sub>_, but as the writer and reader, we are already there, so assume we will name them when we could define the T<sub>L</sub>. No problem! 

Also be aware that compressing is not always in favorite of shrinking! You'll see soon that at some point of abstraction, `x`, the _T<sub>x</sub>_ 's rows will explode to infinity! But those infinite rows are just a simple formula so we consider this process as compression.

For the final point, as the disagreement grows, where we create the _T_ tables, the direction of abstraction itself also is the subject of disagreement, so for more advance description of construction of _Knowledge_, we need `x` in _T<sub>x</sub>_ be some kind of coordinates in a multi-dimensional manifold! But let's establish one successful coordinate, then maybe in the future we could talk more about the other ways to build up our _Knowledge_ based on the steps of adding assumptions/abstractions.

Let's compress more! To do so we need another abstraction, we need functions from Type of combination of _Sum Types_ and _Product Types_ to another Type of  combination of them, where simply means we need maps from a matrix to another matrix. Also we need to define numbers, [Natural numbers](https://en.wikipedia.org/wiki/Natural_number) and [Rational numbers](https://en.wikipedia.org/wiki/Rational_number), to be able to simplify the result of matrix multiplication to a new matrix of numbers. Notice we were able to create matrices when we only had _Sum Types_ and _Product Types_. Additionally we need groups abstraction in the [Group Theory](https://en.wikipedia.org/wiki/Group_theory). Not all of this theory, just the matrix transformations and basics. By this new abstractions we can do the same as the last shrinking but with [Poincaré group](https://en.wikipedia.org/wiki/Poincar%C3%A9_group) as the map function. Therefore we will find in all rows there are at least 4 columns that can be mapped to a standard unit and a [frame of reference](https://en.wikipedia.org/wiki/Frame_of_reference). This will merge all columns of different rulers in different directions or different origins or with different velocities to each other. So huge shrinking. We'll call those 4 columns the position of the observation in the space-time. Let's name the new table _T<sub>2</sub>_.

Before going further let's make it clear for creating [Natural numbers](https://en.wikipedia.org/wiki/Natural_number) we needed [Peano axioms](https://en.wikipedia.org/wiki/Peano_axioms) which can be defined using [Axiom of induction](https://en.wikipedia.org/wiki/Mathematical_induction#Axiom_of_induction) where is the same as [Recursion](https://en.wikipedia.org/wiki/Recursion) that we already assumed by Fractal Hypothesis. So looks like our steps are consistent. Actually more than that! We actually managed to extract [Natural numbers](https://en.wikipedia.org/wiki/Natural_number) abstraction from _TOT_, which is similar to what we can do to all mathematical abstractions. Therefore, someone can spend time to describe how we actually extracted/invented all the math from _TOT_.

It's worth to mention that the rows in _TOT_ can be random observations by a monkey, the same as they were in the early stages of building our _Knowledge_, but as you see in today's world we don't do that. There's a feedback loop from our _Knowledge_ to push the direction of what experiments should be taken. This feedback loop will not affect our construction, by the way!

Furthermore, it's good to avoid infinity as much as we can, because it's not appeared in _TOT_ which is showing that *infinity is not a pattern available in reality*. To achieve that we can argue the [Natural numbers](https://en.wikipedia.org/wiki/Natural_number) assumption above has an upper limit or a max integer number to avoid any kind of infinity, which is achievable because we mapped _Sum Types_, which are finite, to [Natural numbers](https://en.wikipedia.org/wiki/Natural_number) and the matrix transformations applied finite times so in the worse case all those finite transformations piled up, but in the end, all numbers in _T<sub>2</sub>_ are finite numbers, so there is an actual max integer number there.

Additionally, be aware that preserving a pattern by applying a map on other columns is what in Physics we call a [Symmetry](https://en.wikipedia.org/wiki/Symmetry_\(physics\)). For instance, in above explanation we can conclude [Poincaré group](https://en.wikipedia.org/wiki/Poincar%C3%A9_group) is a _Symmetry_ of the _space-time_. You may be familiar with this concept but if not, it's worth mentioning that all Physics' theories are build up on different kinds of _Symmetries_. If you want to enjoy more about this notion, think about the fact that _Fractal Hypothesis_ guarantied the repetition of patterns, where in the other words we can say it guarantied the existence of _Symmetries_.

It's worth mentioning that with _Construction of Knowledge_,  Physics, Chemistry, Biology, etc. have the same access to _TOT_ which means there is no [Hierarchy of Science](https://en.wikipedia.org/wiki/Hierarchy_of_the_sciences) unless someone in a more abstract theory defines it.

# Prediction

The _Fractal Hypothesis_ can predict that we can have those 4 columns of space-time in *every* observation we will ever have, because we observed them in all observation we *ever* had. Woow! The _Fractal Hypothesis_ is the engine for our _Knowledge_ and this new definition of _Science_ revealed it naked! Beautiful!

Let's expand this idea. The _Fractal Hypothesis_ is used to merge columns in our _T<sub>x</sub>_ tables, where `x` is a number in the abstraction axis, but there is another use-case for it. It can predict beyond our _T<sub>x</sub>_ tables. It can predict that the _Symmetry_ of space-time between galaxies far far away is also [Poincaré group](https://en.wikipedia.org/wiki/Poincar%C3%A9_group). We never experimented it but it works well when we dig deeper with our abstractions, such as models, theories, etc. This means we can make more predictions with that prediction! Those predictions will be recorded in _T<sub>x</sub>_ tables and the diversion of those tables started, which means the disagreements grows.

Finally in one of _T<sub>x</sub>_ tables, where we call it _T<sub>R</sub>_ table, we will add the [Real numbers](https://en.wikipedia.org/wiki/Real_number), which are the most non-real numbers, to the collection of our abstractions/assumptions. Notice _T<sub>R-1</sub>_ has finite rows with finite numbers. Then based on _Fractal Hypothesis_ we argue that we can predict the infinite missed rows of the table, so in that case we will have a table with infinite rows. Which is an example of "applying assumptions will not always shrink the table", but it will help to compress it though, as the following explains. Later we define [Derivation](https://en.wikipedia.org/wiki/Derivation_\(differential_algebra\)), etc. to introduce the [Newton's laws of motion](https://en.wikipedia.org/wiki/Newton%27s_laws_of_motion) in _T<sub>N</sub>_ and compress a big part of that infinite table to some formulas. Notice in _T<sub>N</sub>_, for instance, we have forces' column where did not defined in the _TOT_ but we added them while we progressed. Also in case you were not aware when you were working with mathematical formulas such as the second law of motion, they are all infinite matrices as long as you can apply [Taylor](https://en.wikipedia.org/wiki/Taylor_series) series or any other series on them. 

That's happening with other Scientific theories too. So we shrink and shrunk those tables to abstract them more and more with math and formulas to reach the tables we talked about before, such as languages, Philosophy, Astrology, then religions, etc.

Can you see how much we pushed ourselves apart from the _T<sub>0</sub>_ to just reach _T<sub>R</sub>_ table and then _T<sub>N</sub>_,  [Newton's laws of motion](https://en.wikipedia.org/wiki/Newton%27s_laws_of_motion), where previously was accepted them as Science? For instance, one can argue that extending rows of the _T<sub>R</sub>_ table to infinity based on finite rows of _T<sub>R-1</sub>_ will make a problem because we didn't have any infinity pattern in the finite rows of _TOT_, then how is it valid to use the _Fractal Hypothesis_ for that. If you search, you will find a lot of these kind of extensions based on _Fractal Hypothesis_ all over the classical _Science_ definitions, where not all of them are valid deductions after all. Therefore there's no consistent way to say some theories are scientific theories! They are just theories, some of them are more close to _TOT_ and some of them are far far away from it. We can just argue that _G_ in _jT<sub>G</sub>_ for _General Relativity_ is a smaller number respect to _N_ in _T<sub>N</sub>_ for [Newton's laws of motion](https://en.wikipedia.org/wiki/Newton%27s_laws_of_motion).

# Gödel's incompleteness theorems

The [Gödel's incompleteness theorems](https://en.wikipedia.org/wiki/G%C3%B6del%27s_incompleteness_theorems) goes as follow.

> The first incompleteness theorem states that no [consistent system](https://en.wikipedia.org/wiki/Consistency) of [axioms](https://en.wikipedia.org/wiki/Axiom) whose theorems can be listed by an [effective procedure](https://en.wikipedia.org/wiki/Effective_procedure) (i.e., an [algorithm](https://en.wikipedia.org/wiki/Algorithm)) is capable of proving all truths about the arithmetic of [natural numbers](https://en.wikipedia.org/wiki/Natural_number). For any such consistent formal system, there will always be statements about natural numbers that are true, but that are unprovable within the system. The second incompleteness theorem, an extension of the first, shows that the system cannot demonstrate its own consistency.

Do you see its relation to our definition of _Science_ and construction of _Knowledge_? Let me explain! Classically because we needed to prove all statements up to infinity in arithmetic of natural numbers, we needed infinite number of axioms, which cannot be listed by an effective procedure, but look, here the number of rows in _T<sub>0</sub>_ are finite despite that they are huge. Even the number of rows in _T<sub>0</sub>_ to _T<sub>R-1</sub>_ are finite, so we don't need infinite axioms to have a complete and consistent mathematical system here. However, our _Knowledge_ as a mathematical system includes all _T_s, which means infinite number of rows, where as proved by Gödel, it's  incomplete. That's exactly our _Knowledge_ as a whole!

Let's assume the reality is a consistent and complete mathematical system, then the rows in _TOT_ are actually deductions of that system, where can be used as the list of axioms we need to have a consistent and complete abstraction tables, aka _Knowledge_ in a mathematical system sense up to _T<sub>R-1</sub>_. So the good news is our ability to build up _T<sub>0</sub>_ is an _effective procedure_ to list all the axioms we need to hopefully build a consistent and complete mathematical system on top of them. Can you see how beautiful this is? Can you see why current Physics' theories have unsolvable inconsistencies or incompleteness?

# Who are the scientists?

Therefore, we answered what is _Science_. Also it's easy to answer why it's important. Because it's the only source of truth we can have access to, so it's important. Then let's define a _Scientist_. Did you prepare yourself to read a new/better definition for a _Scientist_? Okay!

> Every honest person, who agrees on correctness of observations, in the society is a scientist. However, we have more trustworthy scientists where can be found out by the trust network in a given society. 

Why is it correct? Because every person in the reality have access to parts of _TOT_, so they are scientist if they accept its correctness. But the important part is the trust network that makes some of us more trustworthy _Scientist_, which is the equivalent of classical definition of _Scientist_.

Do you see what's happened? Do you have any clear/available source for the trust network in the current science society? Does papers' impact factor play the role of trust network? Yes in current science society we don't have the trust network, at least a clear and available one, so we had to stick to impact factor, universities' rate, Nobel Prize, etc. and their structures to define the scientists, which I'm not a fan of that approach.

Okay! I accept this is the best we have done so far to create the science society, but please start to build the trust network in a way that it's clear which scientist is the most trusted one, who is the next one and so on. Also this network must be able to clearly, with two/three clicks, answer which group of scientists accepted some particular theory or axiom, etc. If there is any grouping among those scientists, then there should be scientific papers assigned to those groups, which means those group must be considered as publishers, where their impact factors would show how much they are trusted among others. These should be clear and available to everyone.

Personally I was very bad in building my network, especially when I was studying Physics, but logically building up the trust network must be the way to go. However, I am building my network in Software Engineering society, which works perfectly. By the way, eventually I need to build my network in science, and that's why I am writing here.

So it's partly clear that why I am an amateur Physicist. First, because I don't like to be selected in that wrongly defined structures. Second, every new theory starts by a simple guess. For guessing stuff you need a random decision maker in your brain, and random is not truly random, when you try to fit to a structure. So the output is a lot of super clever folks in universities who lack generating new ideas, like scientists in the early 1900s did. Yes! Scientists in the early 1900s had access to a bigger guess space as a consequence of WWs. The effort of current scientists is to fit to the current structure of universities. Even if they are too clever to make no effort to fit in, the random generator inside their brain is affected by the lack of access to fresh seeds! 

By seed, we mean how classical random generators works. The seeds are inputs of any classical random generator. Spending time with the same problem in the same structure, aka universities, would restrict the randomness of the seeds themselves, which will lead us to a smaller guess space to create new theories. So my conclusion after graduation was I want to generate as random guesses as possible to have better options to choose. To do so I started to solving technologies' problem, which are problems of the real world, out there, to expand the seed space of my brain. That's why I am a professional Software Engineer not a professional Physicist. I agree that now I have to fit in Software Engineering society, which again will push different boundaries to my guess space, but these boundaries are off respect to most of professional Physicists. That probably will allow me to thinking out of the box.

Additionally, stress management is the other reason to be a professional Software Engineer rather than a professional Physicist. I can think and talk out of the box without being stressful what can goes wrong in my career!

# What do scientists do?

This session has nothing new but to have a complete view about what is science, it's added here.

Scientists use [Bayesian inference](https://en.wikipedia.org/wiki/Bayesian_inference). It's the tool to work with _T_ tables. In [Bayesian inference](https://en.wikipedia.org/wiki/Bayesian_inference), scientists update the probability distribution of hypothesis to find out which theory is more suitable to newly added observed data. So hypothesis that can hold observed data better will be more acceptable. In _T_ table picture, [Bayesian inference](https://en.wikipedia.org/wiki/Bayesian_inference) is the tool for updating the probability distribution of consistency of abstraction axis respect to emergence of tables from _T<sub>0</sub>_. So scientist use it to find out which direction is the way to go.

# Why Math Works

Let's have a quick fun on one of the consequences of above definition of _Science_. If you followed up the construction of _Knowledge_ from _TOT_, you will be agree that Math, as a part of _Knowledge_, is just some guessed assumptions based on axioms of _TOT_ to detect and extend some patterns. By extend, we mean using _Fractal Hypothesis_ to wrap up repeated patterns to some definitions or assumptions. Therefore, as we assumed the reality is a consistent and complete mathematical system, then those patterns are related to each other, so they will cover each other all the times, and we wonder where is this `pi` doing in my population equation!

By the way, it doesn't answer that Math is discovered or invented, because we discovered some of them and those patterns are consistent, they repeated to generate more patterns, where we call that invention. That invention also would turn out to be consistent with future observations in _TOT_, if we've extended them in the right direction. So Math is both discovered and invented.



---



Hope you have enjoyed reading it, so please feel free to shot your feedback.

# References

 - [Christoffel symbols](https://en.wikipedia.org/wiki/Christoffel_symbols)
 - [Science](https://en.wikipedia.org/wiki/Science)
 - [Scientific Method](https://en.wikipedia.org/wiki/Scientific_method)
 - [Demarcation Problem](https://en.wikipedia.org/wiki/Demarcation_problem)
 - [Falsifiability](https://en.wikipedia.org/wiki/Falsifiability)
 - [Newton's laws of motion](https://en.wikipedia.org/wiki/Newton%27s_laws_of_motion)
 - [Taylor series](https://en.wikipedia.org/wiki/Taylor_series)
 - [General relativity](https://en.wikipedia.org/wiki/General_relativity)
 - [Galileo](https://en.wikipedia.org/wiki/Galileo_Galilei)
 - [Sum Types](https://en.wikipedia.org/wiki/Tagged_union)
 - [Product Types](https://en.wikipedia.org/wiki/Product_type)
 - [Type Theory](https://en.wikipedia.org/wiki/Type_theory)
 - [Fractal](https://en.wikipedia.org/wiki/Fractal#)
 - [Recursion](https://en.wikipedia.org/wiki/Recursion)
 - [Natural numbers](https://en.wikipedia.org/wiki/Natural_number)
 - [Rational numbers](https://en.wikipedia.org/wiki/Rational_number)
 - [Group Theory](https://en.wikipedia.org/wiki/Group_theory)
 - [Poincaré group](https://en.wikipedia.org/wiki/Poincar%C3%A9_group)
 - [Hierarchy of Science](https://en.wikipedia.org/wiki/Hierarchy_of_the_sciences)
 - [Frame of reference](https://en.wikipedia.org/wiki/Frame_of_reference)
 - [Peano axioms](https://en.wikipedia.org/wiki/Peano_axioms)
 - [Axiom of induction](https://en.wikipedia.org/wiki/Mathematical_induction#Axiom_of_induction)
 - [Symmetry](https://en.wikipedia.org/wiki/Symmetry_\(physics\))
 - [Real numbers](https://en.wikipedia.org/wiki/Real_number)
 - [Derivation](https://en.wikipedia.org/wiki/Derivation_(differential_algebra))
 - [Gödel's incompleteness theorems](https://en.wikipedia.org/wiki/G%C3%B6del%27s_incompleteness_theorems)
 - [Bayesian inference](https://en.wikipedia.org/wiki/Bayesian_inference)




