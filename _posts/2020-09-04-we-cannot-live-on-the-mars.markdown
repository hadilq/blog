---
layout:     post
title:      We cannot live on the Mars
date:       2020-09-04
author:     Hadi Lashkari Ghouchani
summary:    We need to choose between being a Type I civilization, in Kardashev scale, or extinct, there’s no other room.
categories: Future
thumbnail:  Mars
tags:
- Mars
- Future
- Type-I-civilization
- Pyramid
---

![Wallpaper](https://hadilq.gitlab.io/blog/assets/2020-09-04-we-cannot-live-on-the-mars-wallpaper.jpeg)

[Mars’ surface gravity](https://en.wikipedia.org/wiki/Gravity_of_Mars) is `3.711 m/s²` and 
[Earth’s surface gravity](https://en.wikipedia.org/wiki/Gravity_of_Earth) is `9.807 m/s²`. That’s it! We cannot live on Mars because its surface gravity is half of what we evolved in for 
millions of years. But we spend billions of dollars for this target. Great! But what if it failed because of a 
simple fact that we ignored?

This article is not about nagging why others would fail, but instead here we want to come up with a simple solution 
that works best for the big problem we have. Earth is going to be destroyed soon if we continue living this way. 
This is the problem we all face if we open our eyes. Is this why we should go to other planets? In my mind, no! We 
need to go to other planets because our population grows exponentially, and we need new resources to make it 
sustainable.

Also, we need to choose between being a Type I civilization, in 
[Kardashev scale](https://en.wikipedia.org/wiki/Kardashev_scale), or extinct, there’s no other room. 
I may explain it more later, but it’s not hard to grasp it.

Nice! So let’s do it! Solution that we want to find needs to satisfy the following.

- Keep Earth itself as intact as possible
- Extendable
- Be scalable
- Achievable

In the following I would explain a solution to our big problem, which I think can keep the Earth intact, extendable 
to other planets, scalable to satisfy exponential growth of our population, and achievable. 

# Solution
The solution starts by answering what is the most common state of the surface of planets in general. You may know 
that they are mostly [covered with gas](https://en.wikipedia.org/wiki/Terrestrial_planet). Earth is a very rare 
exception with a solid surface.

Also thinking about the starting fact in this article you can open the Wikipedia list of Surface gravitates in the 
solar system. Saturn has closest [surface gravity](https://en.wikipedia.org/wiki/Surface_gravity) among all 
of them to the Earth. Also, Saturn is a gas giant, 
which means it doesn't have a defined surface.

## Keep Earth itself as intact as possible
These two facts reach us to the point that **we need to learn how to live in flying cities**. But how can we make one? 
I’ll answer later but if we could we can start living above the oceans on Earth. We can evacuate the continents and let them be intact which is the first goal of this solution. Check!

## Extendable
We already thought about the second goal of this solution which is being extendable to other planets, even beyond 
the solar system. Check! Easy-peasy!

We need to build a balloon like a big ship! A balloon as big as a city or maybe bigger! Such a balloon needs a 
skeleton!

## Scalable
We need to have a structure that scales exponentially as our population grows exponentially on any planet that we 
live on. If you give these conditions to a mathematician, he/she would tell you that you’re looking for a fractal 
city! A fractal is a geometric shape that repeats itself when it scales. So it would be scalable by definition. Check!

## Achievable
To be achievable it needs to be

- Needs a way to construct
- Needs to be stable

So now we need to find a way to construct it!

As big as we need a balloon to be a city, much taller than skyscrapers, it cannot be constructed with tower cranes. 
But it’s scalable, so it can use itself to push its parts up. The solution that I come up with is a [Sierpinski 
square-based pyramid](https://en.wikipedia.org/wiki/Sierpi%C5%84ski_triangle). It’s a fractal. It has slops for each small part that can push up its tip part to the top of 
the pyramid. It has a square base which makes us not deal with wrong angles(Not right angles!) more than we complied.

## Construction
So the construction would be like we have 5 equal Sierpinski square-based pyramids, where we call them parts. We 
arrange them like a flower, where the tip would be in the center of the other 4 parts. Then by pulling those 4 
parts together, the tip would climb up by using the slope of the Sierpinski triangles. Check!

## Stable
If it’s big enough no natural wave exists to match its resonant frequency and excite it. Our plan is to have one that is bigger than a city, so Check!

But as its inside parts can move we need a more advanced solution to make it stable. I’ll come back to this below.

# Proof of concept
We need some simple models of these cities to have an idea what we’re talking about.

## Density
So to make it a balloon we need to vacuum some empty spaces in the middle of 
[Sierpinski square-based pyramid](https://en.wikipedia.org/wiki/Sierpi%C5%84ski_triangle). If 
the height of one of those 5 parts be `h`, and the sides of the base square be `a`, then the volume of each of them 
would be `v0=h*a^2/3`(I’ll not provide an image for formulas because in this way it’s encrypted for geeky eyes, who 
are the target here:D ) and the volume of constructed pyramid would be `v1=2*h(2*a)^2/3=8*ha^2/3=8*v0`. So the middle 
empty space would be `8*v0-5*v0=3v0` and if we just vacuum this empty space the density of the pyramid would decrease 
by a factor of `5/8` every time we scale it up. _Aluminum_ almost has a density of `2800 kg/m³` and air density is `1.225 
kg/m³` so their ratio is `2285.71` then if we scale this pyramid almost for `16` times a pyramid of _Aluminum_ would 
simply float on air. We assumed the skeleton of the pyramid doesn’t have weight, also we counted the density of air 
on Earth’s surface for all altitude, which is much higher than the tip of the pyramid. These two assumptions plus 
_Aluminum_ assumption suggest that we cannot make it!

The atmosphere thickness is almost `480km`. If the height of the final pyramid be `90km` and it scaled up `16` times, 
then its _Aluminum_ building block would almost have a height of `1m`. It’s too small for such a tall building by the 
way! Not what I like! :(

```
e^(ln(2)*ln(2800/1.225)/ln(8/5)) ~ 90000
```

The solution is to float the pyramid in the water, then we master the technology of living in it. So whenever we 
want to build one on Saturn, we would be prepared. However, we may need to scale it like `16` times in Saturn as 
described above! Let’s do the math for this simple model on Earth.

This time again with _Aluminum_ building blocks, but we need to achieve ocean water density, which almost is `1036 
kg/m³`. The ratio is `2.7027`, so it needs to scale up almost `3` times! It’s promising! For Stainless Steel with a 
density of `8000 kg/m³`, we need to scale it just `5` times. Great! So if we scale it up more than `5` times, we 
don’t need to think so much about the densities! Basically it means, the city will float on oceans. Let’s think 
about the scales.

## Scales
Originally we thought about a tall city that we cannot build with tower cranes, so I’m thinking about something 
like `9 km` tall city which is comparable to Mountain Everest. But in the end as the city would float on water, it 
would not be as tall as Everest. If each building block were a house of an average family like `250 m²` area, then 
the sides would be almost `16 m`. Let’s assume its height is the same as its sides, then we need to scale it up 
almost `9` times, which would have `1953125` houses, which would be enough for almost `5` million people. To move 
all the human population we need almost `1400` cities on the ocean! I would say it’s achievable!

# More Details
Great! But a city needs more than houses! Transportation, Water Network, sewage, parks, stadiums, public place 
owners, governments, etc. are needed to be implemented. Let’s think about them one by one!

## Transportation
The idea for transportation is what we can call loops, like [Hyperloop](https://en.wikipedia.org/wiki/Hyperloop) 
which is working on for city to city transportation. They’re elevator/car systems that someone can sit from inside 
a house and commute to another place. No street! No sidewalk! No highway! Just some channels with probably,
[Hilbert curve](https://en.wikipedia.org/wiki/Hilbert_curve), or 
[Moore curve](https://en.wikipedia.org/wiki/Moore_curve) patterns, which 
are scalable fractals, on the basements of each house. People in a car like vehicles can transport inside those 
channels. Also, there can be small size channels, like the size of a pizza box, to transport small things faster, 
which would be good for delivery stuff or mails etc.

## Water Network/Sewage
No need for these kinds of networks, as all these basics can be commuted by tanks in the transportation loop 
automatically. Dedicating to solving it like this would help us to use this automated moving tanks solution to 
tanks full of heavy materials to control the balance of the city. These heavy tanks can be automatically moved 
around the city to cancel out communication of people and stuff around the city to justify the balance. This 
solution is what I mentioned in stable section above.

## Parks/Stadiums
The city is scaled `9` times, which is more than `5` times, and we can make sure we efficiently use light materials to 
build stuff, then the city would float easily even if we decide, based on our need, to make the building blocks 
bigger! For instance, if we decide to make `5` houses together being a building block, then we effectively scale the 
city `9 — 1 = 8` times, which is still more than `5` times, then we can use the empty space in the middle of those `5` 
houses to be gyms, small parks, etc. It’s possible as we calculated above that its volume will be `3v0`, where `v0` 
here is the volume of a house. We can safely do it a couple of more times to use those empty spaces in the middle 
for what a city needs to have like theaters, stadiums, etc.

## Government
Current government models are based on borders. So in the end they belong to the borders not to the people! I was 
looking for something based on [Scientific Method](https://en.wikipedia.org/wiki/Scientific_method) to be designed 
to search for the lifestyle for people. In this sense, we need to experiment different ideas for possible 
lifestyles and have some measurable methods to test them. Also, governments must belong to people. To achieve this 
I propose the following model.

These are the general laws(Constitution):
- Freedom of speech 

- Governments are companies with predefined goals and methods to measure those goals(objectives). Residents are 
  people who signed to practice those methods to achieve those goals. Governments own the public places of part of 
  the colony that their residents are living. The resources that belong to a government linearly depends on the 
  number of residents. 
  
- _Resource manager_ is a software program that more than 80% of people inside the colony agree on using it for 
  dividing resources among the governments.
  
- Freedom of migration. A colony is all the fractal cities in one planet. People are free to sign for any 
  government, and only one government. People who are not signed for any government would follow _Resource manager_ 
  rules. Just those people who live inside the colony can sign for governments. For instance, people who want to 
  live on the Earth surface, there’s no government support, no vote, etc.

No more laws as you can see! The _Resource manager_ is defining every other rule to settle governments and provide 
their resources. Governments emerge to satisfy some promises for people, so they define the rules inside the 
governments based on those goals.

It’s clear that every [Change list(Pull Request)](https://en.wikipedia.org/wiki/Distributed_version_control) to 
merge the resource manager code needs to be elected by all people. The advantage of having Resource manager code is 
that it’s not just the laws, it’s the laws with implementation and guarantee to apply. basically, it’s the proof of 
being applicable for the laws in 
[Curry–Howard correspondence](https://en.wikipedia.org/wiki/Curry%E2%80%93Howard_correspondence) context.

If you look carefully, you would see that except the first law, the rest can be defined in the _Resource manager_ 
code, including how we decide to merge the _Change lists_. So in the end we can have just two statements for all laws,
one law is _Freedom of speech_, that would not change in any circumstances, and another law is the definition of 
_Resource manager_, where would be self containing the whole definitions, applications and how it would change.

We need 80% because we need to count on the _Standard deviation_ of what people want. This means, if we decide based 
on 50%, there are a lot of people who do not agree with it, so it doesn't make sense to merge that idea to Resource 
manager. In the end, if we merge all three last laws to _Resource manager_ one, then this number could be a target to 
change in a Change list.

So in the end, the laws would be
- Freedom of speech
- A code for Resource manager, with all access exists. There are a lot of accesses that will not be exposed to 
  anything else, code or human.

To clarify the overall process, some bunch of people propose an idea for a lifestyle, and measuring methods for 
some goals in that lifestyle. They can start a government, then people would sign for that government and based on 
the number of people, and the minimum threshold of people(defined in the _Resource manager_ code), the government 
would start working by migrating those people to a place that _Resource manager_ defined and own the public places of 
that area. If people feel that there is a better government out there, so they sign for that and migrate away from 
this government and in case the number of residents goes less than that threshold, the government would be finished 
working, and the rest of people have to sign for another government. This life-cycle of governments must happen to 
all of them, and if it’s not, then we ran out of better ideas for living, which is dangerous IMHO!

People who signed to be residents of a government have to follow the rules that the government defines among people,
which include defining permissions, economical rules, money, courts, etc. To define the rules among governments, 
they can create or join to some agreements, so they’re free to accept their relationship with other governments and 
courts, etc.

As I’m a Software Engineer, I can think about having multi independent layers of recovery plan, in case of security 
risks or having bugs. Also may have a very big plan to update everything based on their stable source code. For 
instance, compiler code needs to be old enough to make sure it’s stable, then compile it inside the isolated 
micro-services of Resource manager and use it to compile the rest of the project, then deploy it!

Here I hope I defined it well enough to be applicable!

These are parts of my before sleep thoughts, where me and my wife named them Hadiapolis :D There are more, but 
let’s see what people’s reaction is.

# References
- [Gravity of Earth](https://en.wikipedia.org/wiki/Gravity_of_Earth)
- [Gravity of Mars](https://en.wikipedia.org/wiki/Gravity_of_Mars)
- [Kardashev scale](https://en.wikipedia.org/wiki/Kardashev_scale)
- [Terrestrial planet](https://en.wikipedia.org/wiki/Terrestrial_planet)
- [Surface gravity](https://en.wikipedia.org/wiki/Surface_gravity)
- [Sierpiński triangle](https://en.wikipedia.org/wiki/Sierpi%C5%84ski_triangle)
- [Metals and Alloys — Densities](https://www.engineeringtoolbox.com/metal-alloys-densities-d_50.html)
- [Hyperloop](https://en.wikipedia.org/wiki/Hyperloop)
- [Hilbert curve](https://en.wikipedia.org/wiki/Hilbert_curve)
- [Moore curve](https://en.wikipedia.org/wiki/Moore_curve)
- [Scientific Method](https://en.wikipedia.org/wiki/Scientific_method)
- [Distributed version control](https://en.wikipedia.org/wiki/Distributed_version_control)
- [Curry–Howard correspondence](https://en.wikipedia.org/wiki/Curry%E2%80%93Howard_correspondence)

# Original Post
This article originally posted on [Medium](https://medium.com/predict/we-cannot-live-on-the-mars-3d138c509f2b).

